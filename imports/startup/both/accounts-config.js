import { Accounts } from 'meteor/accounts-base';
import { check } from 'meteor/check';

// on client
if (Meteor.isClient) {
  Accounts.ui.config({
    passwordSignupFields: 'USERNAME_AND_OPTIONAL_EMAIL',
  });

}

// on server
if (Meteor.isServer) {
  Meteor.users.allow({
  	  update: function () { return true; },
  });
  Meteor.methods({
    'accounts.changeEmail'(email) {
        check(email, String);

        // Make sure the user is logged in before inserting a message
        if (! this.userId) {
          throw new Meteor.Error('not-authorized');
        }

        // add email
        if(email.length>0){
          Accounts.addEmail(this.userId, email.toLowerCase());
        }

    },
    'accounts.changePassword'(newPassword) {
        check(newPassword, String);

        // Make sure the user is logged in before inserting a message
        if (! this.userId ) {
          throw new Meteor.Error('not-authorized');
        }
        if(newPassword.length<1){
          return;
        }

        // change password
        return Accounts.setPassword(this.userId, newPassword);
        console.log('pass ', newPassword);
      },
  });
  Accounts.onCreateUser(function(options, user) {
      user.profile = {
        'fullName' : '',
        'email' : '',
        'location' : '',
      };
      return user;
  });

}
