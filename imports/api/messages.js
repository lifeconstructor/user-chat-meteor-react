import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Messages = new Mongo.Collection('messages');

if (Meteor.isServer) {

  // This code only runs on the server
  // Only publish messages that are public or belong to the current user
  Meteor.publish('messages', function publishMassages() {
    // should be logged in before view a messages
    return Messages.find({
      // $or: [
      //   { private: { $ne: true } },
      //   { owner: this.userId },
      // ],
    });
  });
}

Meteor.methods({

  'messages.insert'(text, location) {
    check(text, String);
    check(location, String);

    // Make sure the user is logged in before inserting a message
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    Messages.insert({
      text: text,
      createdAt: new Date(),
      owner: Meteor.user()._id,
      checked: false,
      username: Meteor.user().username,
      location: location,
    });
  },

  'messages.remove'(messageId) {
    check(messageId, String);

    const message = Messages.findOne(messageId);
    if (message.owner !== this.userId) {
      // If the massage is private, make sure only the owner can delete it
      throw new Meteor.Error('not-authorized');
    }

    Messages.remove(messageId);
  },

  'messages.setChecked'(messageId, setChecked) {
    check(messageId, String);
    check(setChecked, Boolean);

    const message = Messages.findOne(messageId);
    if (message.private && message.owner !== this.userId) {
      // If the message is private, make sure only the owner can check it off
      throw new Meteor.Error('not-authorized');
    }

    Messages.update(messageId, { $set: { checked: setChecked } });
  },

  'messages.setPrivate'(messageId, setToPrivate) {
    check(messageId, String);
    check(setToPrivate, Boolean);

    const message = Messages.findOne(messageId);

    // Make sure only the message owner can make a message private
    if (message.owner !== this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    Messages.update(messageId, { $set: { private: setToPrivate } });
  },

  'messages.updateLocation'(location) {
    check(location, String);
    var usern = Meteor.users.findOne(this.userId).username;

    check(usern, String);

    let mesgs = Messages.find({
      $and : [ {username: { $eq: usern }}, {location: { $eq: '' }}]
    });

    mesgs.forEach(function(Message){
      Messages.update(Message._id, { $set: { location: location } });
    });
  },
});
