import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
// add collections
import { createContainer } from 'meteor/react-meteor-data';
// api
import { Messages } from '../api/messages.js';
import '../startup/both/accounts-config.js';

// UserProfile component
class UserProfile extends Component {

  constructor(props) {
    super(props);

    this.state = {
      hideProfile: this.props.hideProfile,
    };

  }

  submitUserProfile(event) {
    event.preventDefault();
    // Get value from form element
    const fullName = ReactDOM.findDOMNode(this.refs.fullName).value.trim();
    const email    = ReactDOM.findDOMNode(this.refs.email).value.trim();
    const newPassword = ReactDOM.findDOMNode(this.refs.new_password).value.trim();
    let location = ReactDOM.findDOMNode(this.refs.location_select).value.trim();
    location = (this.location() && location=="")?this.location():location;
    console.log(email, newPassword);

    // update old messages with new (first time) user's location
    if(this.props.currentUser.profile.location.length == 0){
      Meteor.call('messages.updateLocation', location);
    }
    // update user email
    Meteor.call('accounts.changeEmail', email, function(err, res){
      console.log(err, res);
    });
    // update user password
    Meteor.call('accounts.changePassword', newPassword, function(err, res){
      console.log(err, res);
    });
    // update user profile
    this.props.users.update( this.props.currentUser._id , {
      $set : { profile : {
        'fullName'  : fullName,
        'email'     : email,
        'location'  : location,
      }}
    });

  }

  location() {
    return this.props.currentUser.profile.location;
  }

  showForm(){
    let locations = ["Dnipro", "Kiev", "Kharkov", "Lviv", "Odessa"];

    let user = this.props.currentUser;
    let fullName = (typeof(user.profile.fullName)=='undefined')?'':user.profile.fullName;
    let email =  (typeof(user.profile.email)=='undefined')?'':user.profile.email;
    return(
      <div>
        <h1>User Profile</h1>
        <form onSubmit={this.submitUserProfile.bind(this)} className="user-profile">
          <p>
              <input type="text" ref="fullName" defaultValue={fullName} placeholder="Full Name" />
          </p>
          <p>
              <input type="text" ref="email" defaultValue={email} placeholder="Email" />
          </p>
          <p>
              <input type="text" ref="new_password" placeholder="New Password" />
          </p>

          <p>
            <select ref="location_select" defaultValue="">
                <option value="" disabled>
                  {this.location() ? this.location() : 'Chat Location'}
                </option>
                {
                  locations.map(function(loc){
                    return <option key={loc} value={loc}>{loc}</option>
                  })
                }
            </select>
          </p>
          <p><button>Save</button></p>
        </form>
      </div>
    );
  }

  render() {

    return (
      <div>
        <label className="profile-on-off">
          Edit Your Profile
          <input
            type="checkbox"
            readOnly
            checked={this.props.hideProfile}
            onClick={this.props.toggleHideProfile.bind(this, 1)}
          />
        </label>
        {this.props.hideProfile ? this.showForm() : ''}

        <p></p>

    </div>
    );
  }
}

// use with collection
UserProfile.propTypes = {
  currentUser: PropTypes.object,
  users: PropTypes.object,
  hideProfile: PropTypes.bool,
};
// receive sync collections
export default createContainer(() => {
  return {
    currentUser: Meteor.user(),
    users: Meteor.users,
  };
}, UserProfile);
