import React, { Component, PropTypes } from 'react';
// react dom
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
// add collections
import { createContainer } from 'meteor/react-meteor-data';
// messages collection
import { Messages } from '../api/messages.js';
// sub template Message
import Message from './Message.jsx';
// sub template UserProfile
import UserProfile from './UserProfile.jsx';
// meteor account-user-UI
import AccountsUIWrapper from './AccountsUIWrapper.js';


// App component - represents the whole app
class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      hideCompleted: false,
      countMessages: 0,
      hideProfile: false,
    };

  }

  handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    const text = ReactDOM.findDOMNode(this.refs.textInput).value.trim();
    const location = this.props.currentUser.profile.location;

    Meteor.call('messages.insert', text, location);

    // Clear form
    ReactDOM.findDOMNode(this.refs.textInput).value = '';
  }

  toggleHideCompleted() {
    this.setState({
      hideCompleted: !this.state.hideCompleted,
    });
  }

  renderMessages() {
    let filteredMessages = this.props.messages;
    var ul = Meteor.user()['profile']['location'];

    if (this.state.hideCompleted) {
      filteredMessages = filteredMessages.filter(message => !message.checked);
    }
    filteredMessages = filteredMessages.filter(function(message){
      return message.location === ul;
    });

    return filteredMessages.map((message) => {
      const currentUserId = this.props.currentUser && this.props.currentUser._id;
      const showPrivateButton = message.owner === currentUserId;

      return (
        <Message
          key={message._id}
          message={message}
          showPrivateButton={showPrivateButton}
        />
      );
    });
  }

  // toggle state from <UserPortfolio /> or from <App /> (here)
  toggleHideProfile(parentOrChild){
    this.setState({hideProfile: (parentOrChild==1)?(!this.state.hideProfile):false});
    //console.log('hideProfile: ',parentOrChild,this.state.hideProfile);
  }

  render() {
    return (
      <div className="container">

        <header>
          <div onClick={this.toggleHideProfile.bind(this, 0)}>
            <AccountsUIWrapper />
          </div>

          <UserProfile
            hideProfile={this.state.hideProfile}
            toggleHideProfile={this.toggleHideProfile.bind(this)}/>

          <h1>Messages List</h1>

          <label className="hide-completed">
            Hide Read Messages
            <input
              type="checkbox"
              readOnly
              checked={this.state.hideCompleted}
              onClick={this.toggleHideCompleted.bind(this)}
              />
          </label>

          { this.props.currentUser ?
            <form className="new-message" onSubmit={this.handleSubmit.bind(this)} >
              <input
                type="text"
                ref="textInput"
                placeholder="Type to add new Messages"
              />
            </form> : ''
          }
        </header>

        <ul>
          {this.props.currentUser ? this.renderMessages() : ''}
        </ul>
      </div>
    );
  }
}

App.propTypes = {
  messages: PropTypes.array.isRequired,
  //incompleteCount: PropTypes.number.isRequired,
  currentUser: PropTypes.object,
};

export default createContainer(() => {
  Meteor.subscribe('messages');
  return {
    messages: Messages.find({}, { sort: { createdAt: -1 } }).fetch(),

    // incompleteCount: Messages.find({
    //   checked: { $ne: true }
    // }).count(),

    currentUser: Meteor.user(),
  };
}, App);
