import React, { Component, PropTypes } from 'react';
import { Meteor } from 'meteor/meteor';
import { Messages } from '../api/messages.js';
import classnames from 'classnames';

// Message component - represents a single todo item
export default class Message extends Component {

  toggleChecked() {
    // Set the checked property to the opposite of its current value
    Meteor.call('messages.setChecked', this.props.message._id, !this.props.message.checked);
  }

  deleteThisMessage() {
    Meteor.call('messages.remove', this.props.message._id);
  }

  togglePrivate() {
    Meteor.call('messages.setPrivate', this.props.message._id, ! this.props.message.private);
  }

  prefix_zero(hour_or_minute) {
    return parseInt(hour_or_minute) < 10 ? '0'+hour_or_minute : hour_or_minute;
  }

  render() {
    // Give Messages a different className when they are checked off,
    // so that we can style them nicely in CSS
    const MessageClassName = classnames({
      checked: this.props.message.checked,
      private: this.props.message.private,
    });

    // for testing - http://jsbin.com/vasuxu/3/embed?html,js,output
    let cdate = new Date(this.props.message.createdAt);

    return (
      <li className={MessageClassName}>

        <button className="time">
          {this.prefix_zero(cdate.getHours())}:{this.prefix_zero(cdate.getMinutes())}
          &nbsp;({cdate.getDate()}.{cdate.getMonth()})
          <span className="delete" onClick={this.deleteThisMessage.bind(this)}>.</span>
        </button>

        <input
          type="checkbox"
          readOnly
          checked={this.props.message.checked}
          onClick={this.toggleChecked.bind(this)}
        />

        { false && this.props.showPrivateButton ? (
          <button className="toggle-private" onClick={this.togglePrivate.bind(this)}>
            { this.props.message.private ? 'Private' : 'Public' }
          </button>
        ) : ''}


        <span className="text">
          <strong>{this.props.message.username}</strong>: {this.props.message.text}
        </span>
      </li>
    );
  }
}

Message.propTypes = {
  // This component gets the Message to display through a React prop.
  // We can use propTypes to indicate it is required
  message: PropTypes.object.isRequired,
  showPrivateButton: React.PropTypes.bool.isRequired,
};
