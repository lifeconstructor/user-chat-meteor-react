import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import '../imports/ui/stylesheets/body.css';
import App from '../imports/ui/App.jsx';
import '../imports/startup/both/accounts-config.js';

Meteor.startup(() => {
  render(<App />, document.getElementById('render-target'));
});
